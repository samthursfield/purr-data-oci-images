# Purr Data container images

This repository builds container images suitable for building and testing
Purr Data.

Images are pushed to this project's [Docker
registry](https://gitlab.com/samthursfield/purr-data-oci-images/container_registry)
automatically when changes are merged to the 'master' branch.

## Using the images in CI

To use these images in a GitLab CI pipeline, set the `image` key in the
`.gitlab-ci.yml` file to point to one of the images in this project's
repository:

    image: registry.gitlab.com/samthursfield/purr-data-oci-images/amd64/debian:stretch

## Using the images locally

You can also try out images on your local machine. Here is an example which
opens a shell inside a Docker container:

    docker run -i -t registry.gitlab.com/samthursfield/purr-data-oci-images/amd64/debian:stretch /bin/bash

# Related projects

Albert Graef's [OBS repository for Purr Data](https://build.opensuse.org/package/show/home:aggraef/purr-data)
contains packaging instructions for Purr Data on various Linux flavours.

This repo is based on [librsvg-oci-images](https://gitlab.gnome.org/GNOME/librsvg-oci-images/).
