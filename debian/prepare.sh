#!/bin/sh

set -ex

# Avoid interactive prompts!
export DEBIAN_FRONTEND=noninteractive

apt-get -yq update && apt-get -yq upgrade

# Dependencies from https://build.opensuse.org/package/show/home:aggraef/purr-data
apt-get -yq install autoconf automake bison debhelper dh-python flex \
                    flite1-dev ladspa-sdk libasound2-dev libavifile-0.7-dev \
                    libbluetooth-dev libcairo2-dev libdc1394-22-dev libdv4-dev \
                    libfftw3-dev libfluidsynth-dev libftgl-dev libgconf2-dev \
                    libgl-dev libgl1-mesa-dev libglew-dev libgmerlin-avdec-dev \
                    libgsl-dev libgsm1-dev libgtk-3-dev libgtk2.0-dev \
                    libiec61883-dev libjack-dev libjpeg-dev liblua5.3-dev \
                    libmagick++-dev libmp3lame-dev libmpeg3-dev libnss3-dev \
                    libquicktime-dev libraw1394-dev libsmpeg-dev libspeex-dev \
                    libstk0-dev libtiff5-dev libtool libv4l-dev libvorbis-dev \
                    libxss-dev libxtst-dev libxv-dev libxxf86vm-dev pkg-config \
                    python-dev rsync zlib1g-dev

# Build script dependencies
apt-get -yq install git wget

# .gitlab-ci.yml script dependencies
apt-get -yq install ccache sudo valgrind

# Run tests as a normal user, not 'root'.
useradd -Um purrdata
